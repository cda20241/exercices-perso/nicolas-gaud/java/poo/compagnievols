package org.monEntreprise;

import java.util.Date;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {

    public static void main(String[] args) {
        // Press Alt+Entrée with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        Compagnie compagnie1 = new Compagnie("A");
        Compagnie compagnie2 = new Compagnie("B");
        Avion avion = new Avion("AirPlane", 10);
        compagnie1.creerVol("Berlin", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18), avion);
        compagnie1.creerVol("Paris", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18), avion);
//        compagnie1.afficherVols();
//        List<Vol> volsParis = compagnie1.getVolsByVilleDepart("Paris");
//        for (Vol vol : volsParis) {
//            compagnie1.annulerVol(vol);
//        }
//        System.out.println("Vols pour Paris annulés");
//        compagnie1.afficherVols();

        // Création de 3 billets pour le premier vol de la compagnie1
        for (int i = 0 ; i < 3 ; i++){
            compagnie1.getVols().get(Constantes.FIRST_FLIGHT).creerBillet(Constantes.TICKET_PRICE, compagnie1.getVols().get(Constantes.FIRST_FLIGHT).getAvion().getSieges().get(i));
        }

        boolean isFree = false;
        Scanner scanner = new Scanner(System.in);
        String place = "";
        Siege siege = null;
        // Menu et vérifivation de l'attribution d'un siège entré par l'utilisateur
        while (!isFree){
            System.out.println("Veuillez choisir une place : ");
            place = scanner.nextLine();
            if (!compagnie1.getVols().get(Constantes.FIRST_FLIGHT).checkSeatExists(new Siege(place.substring(0,1), Integer.parseInt(place.substring(1)))
                    , compagnie1.getVols().get(Constantes.FIRST_FLIGHT).getAvion().getSieges())){
                System.out.println("Ce siege nexiste pas !");
            }
            else{
                siege = compagnie1.getVols().get(Constantes.FIRST_FLIGHT).attribuerSiege(place, compagnie1.getVols().get(0).rechercherPlacesLibres());
                if (siege != null){
                    System.out.println("Ce siege est libre !");
                    isFree = true;
                }
                else {
                    System.out.println("Ce siege est déjà attribué !");
                }
            }
        }
        Passager passager = new Passager("DUPONT", "Jean");
        passager.getBillets().add(compagnie1.getVols().get(Constantes.FIRST_FLIGHT).creerBillet(Constantes.TICKET_PRICE, siege));
        System.out.println("Votre billet a bien été créé.");

        Avion avionREmplacement = new Avion("AirForceOne", 20);
        compagnie1.changerAvionVol("AirPlane", avionREmplacement);
        System.out.println("L'avion \"AirPlane a été remplacé par l'avion \"AirForceOne.");
    }
}