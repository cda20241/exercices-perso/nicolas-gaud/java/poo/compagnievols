package org.monEntreprise;

import java.util.ArrayList;
import java.util.Date;

public class Vol {
    private Integer numVol;
    private Date dateDepart;
    private Date dateArrivee;
    private String villeDepart;
    private String villeArrivee;
    private Compagnie compagnie;
    private Avion avion;
    private ArrayList<Billet> billets;

    public Vol(Integer numVol, Date dateDepart, Date dateArrivee, String villeDepart, String villeArrivee, Avion avion) {
        this.numVol = numVol;
        this.dateDepart = dateDepart;
        this.dateArrivee = dateArrivee;
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
        this.avion = avion;
        this.billets = new ArrayList<>();
    }

    @Override
    public String toString() {
        return this.numVol + " " + this.villeDepart + " " + this.villeArrivee + " " + this.dateDepart + " " + this.dateArrivee + " " + this.avion;
    }

    public Integer getNumVol() {
        return numVol;
    }

    public Date getDateDepart() {
        return dateDepart;
    }

    public Date getDateArrivee() {
        return dateArrivee;
    }

    public String getVilleDepart() {
        return villeDepart;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public Compagnie getCompagnie() {
        return compagnie;
    }

    public Avion getAvion() {
        return avion;
    }

    public void setNumVol(Integer numVol) {
        this.numVol = numVol;
    }

    public void setDateDepart(Date dateDepart) {
        this.dateDepart = dateDepart;
    }

    public void setDateArrivee(Date dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public ArrayList<Billet> getBillets() {
        return this.billets;
    }

    public void setBillets(ArrayList<Billet> billets) {
        this.billets = billets;
    }

    public Billet creerBillet(Integer prix, Siege siege){
        Billet billet = new Billet(prix, siege);
        this.billets.add(billet);
        return billet;
    }

    public void afficherBillets(){
        for (Billet billet : this.billets){
            System.out.println(billet.getSiege().getNumAllee() + " : " + billet.getSiege().getNumRangee());
        }
    }

    /**
     * Cette fonction compare les sièges attribués dans les billets du vol avec ceux de l'avion du vol
     * et affiche ceux qui ne sont pas déjà attribués
     */
    public ArrayList<Siege> rechercherPlacesLibres(){
        ArrayList<Siege> sieges = this.avion.getSieges();
        for (Billet billet : this.billets){
            sieges.removeIf(siege -> (billet.getSiege().getNumAllee()
                    + String.valueOf(billet.getSiege().getNumRangee())).equalsIgnoreCase(siege.getNumAllee()
                    + String.valueOf(siege.getNumRangee())));
        }
        return sieges;
    }

    /**
     * Cette fonction vérifie si un siège entré par l'utilisateur et passé en paramètre existe dans l'avion affecté au vol
     * @param siege Siege
     * @param sieges ArrayList<Siege>
     * @return boolean
     */
    public boolean checkSeatExists(Siege siege, ArrayList<Siege> sieges){
        boolean exists = false;
        for (Siege siegeTmp : sieges){
            if (siegeTmp.getNumAllee().equalsIgnoreCase(siege.getNumAllee()) && siegeTmp.getNumRangee().equals(siege.getNumRangee())){
                exists = true;
                break;
            }
        }
        return exists;
    }

    /**
     * Cette fonction verifie que le siege definit par "place" entré par l'utilisateur existe dans la liste de sièges
     * non attribués passée en paramètre
     * @param place String
     * @param sieges Arrralist<Siege>
     * @return
     */
    public Siege attribuerSiege(String place, ArrayList<Siege> sieges) {
        String a = place.substring(0, 1);
        String b = place.substring(1);
        for (Siege tmpSiege : sieges) {
            if (tmpSiege.getNumAllee().equalsIgnoreCase(a) && tmpSiege.getNumRangee().equals(Integer.parseInt(b)))
                return tmpSiege;
        }
        return null;
    }
}

