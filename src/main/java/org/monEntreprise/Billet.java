package org.monEntreprise;

public class Billet {
    private Integer prix;
    private Siege siege;

    public Billet(Integer prix) {
        this.prix = prix;
    }
    public Billet(Integer prix, Siege siege) {
        this.prix = prix;
        this.siege = siege;
    }

    public Integer getPrix() {
        return prix;
    }

    public Siege getSiege() {
        return siege;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public void setSiege(Siege siege) {
        this.siege = siege;
    }
}
