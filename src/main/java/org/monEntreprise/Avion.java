package org.monEntreprise;

import java.util.ArrayList;
import java.util.List;

public class Avion {
    private String modele;
    private ArrayList<Siege> sieges;

    public Avion(String modele, int nbRangees) {
        this.modele = modele;
        this.sieges = new ArrayList<>();
        // Création des sièges A1 - A -> nbRangees à I1 - I -> nbRangees
        // 65 = "A" et 73 = "I" en Ascii décimal
        for (int i = 65 ; i <= 73 ; i++){
            for (int j = 1 ; j <= nbRangees ; j++){
                Siege siege = new Siege(Character.toString(i), j);
                this.sieges.add(siege);
            }

        }
    }

    public String getModele() {
        return modele;
    }

    public ArrayList<Siege> getSieges() {
        return sieges;
    }

    public void afficherSieges(){
        for (Siege siege : sieges){
            System.out.println(siege.getNumAllee() + " : " + siege.getNumRangee());
        }
    }
}
