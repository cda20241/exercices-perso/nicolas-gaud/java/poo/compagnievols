package org.monEntreprise;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Passager {

    private String nom;
    private String prenom;
    private Date dateNaissance;
    private String telephone;
    private String status;
    private List<Billet> billets = new ArrayList<>();

    public Passager(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
        this.billets = new ArrayList<>();
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return this.dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Billet> getBillets() {
        return this.billets;
    }

    public void setBillets(List<Billet> billets) {
        this.billets = billets;
    }
}
